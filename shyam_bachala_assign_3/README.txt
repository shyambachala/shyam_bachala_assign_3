## To clean:
ant -buildfile src/build.xml clean

-----------------------------------------------------------------------
## To compile:

ant -buildfile src/build.xml all

Description: Compiles your code and generates .class files inside the BUILD
folder.

-----------------------------------------------------------------------

## To run by code:

ant -buildfile src/build.xml run -Darg0=input.txt -Darg1=output.txt -Darg2=DEBUG_VALUE

Description:

Testing the input.txt file and delete.txt has been done by placing it in the [shyam_bachala_assign_3/airportSecurityState]
as mentioned in the design requirements.

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
## To create tarball for submission

ant -buildfile src/build.xml tarzip

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
## EXPLANATION OF THE OUTPUT

Sample input.txt
 Day:1;TOD:10:00;Airline:United;Item:ShavingBrush
 Day:2;TOD:07:30;Airline:JetBlue;Item:NailCutter
 Day:2;TOD:11:00;Airline:Delta;Item:Gun
 Day:2;TOD:16:30;Airline:AmericanAirlines;Item:Wine
 Day:2;TOD:17:00;Airline:CapeAir;Item:Knife
 Day:2;TOD:18:50;Airline:Southwest;Item:Blade

PARAMETERS FOR DECIDING THE STATE:
  LOW_RISK
    0 = average traffic per day < 4 OR
    0 = average prohibited items per day < 1
  MODERATE_RISK
    4 = average traffic per day < 8 OR
    1 = average prohibited items per day < 2
  HIGH_RISK
    average traffic per day >= 8 OR
    average prohibited items per day >= 2

VALUES OF PARAMETERS AFTER READING EACH LINE AND DECIDING THE STATE:
 NUM# TRAVELLERS: 1	NUM# PROHIBITED_ITEMS: 0	NUM# DAYS: 1   --> avg traffic per day = 1 ; avg prohibited items per day = 0  --> Low risk
 NUM# TRAVELLERS: 2	NUM# PROHIBITED_ITEMS: 1	NUM# DAYS: 2   --> avg traffic per day = 1 ; avg prohibited items per day = 0  --> Low risk
 NUM# TRAVELLERS: 3	NUM# PROHIBITED_ITEMS: 2	NUM# DAYS: 2   --> avg traffic per day = 1 ; avg prohibited items per day = 1  --> Change:Moderate risk(prohibited item = 1)
 NUM# TRAVELLERS: 4	NUM# PROHIBITED_ITEMS: 2	NUM# DAYS: 2   --> avg traffic per day = 2 ; avg prohibited items per day = 1  --> Moderate risk
 NUM# TRAVELLERS: 5	NUM# PROHIBITED_ITEMS: 3	NUM# DAYS: 2   --> avg traffic per day = 2 ; avg prohibited items per day = 1  --> Moderate risk
 NUM# TRAVELLERS: 6	NUM# PROHIBITED_ITEMS: 4	NUM# DAYS: 2   --> avg traffic per day = 3 ; avg prohibited items per day = 2  --> Change:High risk(prohibited items = 2)

Sample output.txt
 1 3 5 7 9
 1 3 5 7 9
 2 3 5 8 9
 2 3 5 8 9
 2 3 5 8 9
 2 4 6 8 10
--------------------------------------------------------------------------------------------------------------------------------------------------------------

