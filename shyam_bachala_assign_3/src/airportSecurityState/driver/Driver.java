package airportSecurityState.driver;

import airportSecurityState.airportStates.inputProcesser;
import airportSecurityState.util.FileProcessor;
import airportSecurityState.util.MyLogger;

import java.io.File;

/**
 * Driver Class.
 */
public class Driver 
{

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) 
	{
		if(args.length != 3)
		{
			System.out.println("INVALID NUMBER OF INPUT PARAMETERS: " + args.length + "\nGIVE INPUT AS BELOW\n" 
		                         + "ant -buildfile src/build.xml run -Darg0=/home/input_file/input.txt -Darg1=/home/input_file/output.txt -Darg2=DEBUG_VALUE");
			System.exit(4);
		}
		
		
//		LOCAL VARIABLES
		String inputLine;
		//INPUT FILE
		File infile = new File(args[0]);
		//OUTPUT FILE
		File outfile1 = new File(args[1]);
		
		
//		CHECKING INPUT.TXT IS EMPTY
		if(infile.length() == 0)
		{
			System.out.println("ERROR: input.txt IS EMPTY");
			System.exit(2);
		}

		
//		FILE READING AND SENDING THE READ LINE TO THE INPUT_PROCESSOR CLASS
		MyLogger.setDebugValue(Integer.parseInt(args[2]));
		FileProcessor freader = new FileProcessor(infile, true);    
		inputProcesser in_process = new inputProcesser(outfile1);
		
		while((inputLine = freader.readLine()) != null)
	    {
			try
			{
				in_process.getString(inputLine);
				in_process.process();
			}
			catch(Exception e)
			{
				System.out.println("\nException : inputProcesser class process method Error\n");
       			e.printStackTrace();
			}
			finally {}
	    }
		
//		REMOVE THE FILE STREAMS
	    freader.remove_dev();
	}

}
