package airportSecurityState.airportStates;

import java.io.File;

import airportSecurityState.util.MyLogger;
import airportSecurityState.util.Results;
import airportSecurityState.util.MyLogger.DebugLevel;

/**
 * inputProcesser Class.
 */
public class inputProcesser 
{
	
	/** input line. */
	String inputLine;
	
	/** number of travelers. */
	int number_travellers;
	
	/** number of prohibited items. */
	int number_prohibited_items;
	
	/** number of days. */
	int number_days;
	
	/** current day. */
	String current_day;
	
	/** first day. */
	boolean first_day;
	
	/** securityFactors type variable. */
	SecurityFactors airport_security;
	
	/**  result class type variable. */
	Results result_airport_states;
	
	/**  output file. */
	File out;
	
	/**
	 * inputProcesser constructor.
	 *
	 * @param outFile  write file
	 */
	public inputProcesser(File outFile)
	{
		out = outFile;
		inputLine = "";
		number_travellers = 0;
		number_prohibited_items = 0;
		number_days = 0;
		current_day = "";
		first_day = false;
		airport_security = new SecurityFactors();
		result_airport_states = new Results();
		MyLogger.writeMessage("inputProcessor Class Constructor has been called", DebugLevel.CONSTRUCTOR);
	}
	
	/**
	 * Gets  string.
	 *
	 * @param in  input string
	 */
	public void getString(String in)
	{
		try
		{
			inputLine = in;
		}
		catch(Exception e)
		{
			System.out.println("\nException : InputLine read Error\n");
   			e.printStackTrace();
		}
		finally
		{}
	}
	
	/**
	 * Process: Calculates the parameters needed to decide the state
	 */
	public void process() 
	{
		String[] node_inputs = inputLine.split(";");
		String day = node_inputs[0].replaceAll("Day:", "");
		String item = node_inputs[3].replaceAll("Item:", "");
		number_travellers++;
   	
		if(item.equals("Gun") || item.equals("NailCutter") || item.equals("Blade") || item.equals("Knife"))
		{
			number_prohibited_items++;
		}
		if(!first_day)
		{
			number_days++;
			current_day = day;
			first_day = true;
		}
		else
		{
			if(!current_day.equals(day))
			{
				number_days++;
				current_day = day;
			}
		}
		
		MyLogger.writeMessage("NUM# TRAVELLERS: " + Integer.toString(number_travellers) + "\tNUM# PROHIBITED_ITEMS: " + Integer.toString(number_prohibited_items) + "\tNUM# DAYS: " + Integer.toString(number_days)  , DebugLevel.STATS);
		airport_security.perform(result_airport_states, number_travellers/number_days, number_prohibited_items/number_days);
		result_airport_states.writeToFile(out);
	}
}
