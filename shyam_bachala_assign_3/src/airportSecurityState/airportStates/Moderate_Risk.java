package airportSecurityState.airportStates;

import airportSecurityState.util.MyLogger;
import airportSecurityState.util.MyLogger.DebugLevel;

/**
 * Moderate_Risk Class.
 */
public class Moderate_Risk implements AirportStateI 
{
	
	/** SecurityFactors variable */
	SecurityFactors airport_security;
	
	/**
	 * Moderate risk class constructor.
	 *
	 * @param securityFactors type object
	 */
	public Moderate_Risk(SecurityFactors securityFactors) 
	{
		airport_security = securityFactors;
		MyLogger.writeMessage("Moderate Risk State Class Constructor has been called", DebugLevel.CONSTRUCTOR);
	}

	/* 
	 * OperationID_1() from AirportSecurityI
	 */
	public String OperationID_1() 
	{
		return "";
	}

	/* 
	 * OperationID_2() from AirportSecurityI
	 */
	public String OperationID_2() 
	{
		return "2";
	}

	/* 
	 * OperationID_3() from AirportSecurityI
	 */
	public String OperationID_3() 
	{
		return " 3";
	}

	/* 
	 * OperationID_4() from AirportSecurityI
	 */
	public String OperationID_4() 
	{
		return "";
	}

	/* 
	 * OperationID_5() from AirportSecurityI
	 */
	public String OperationID_5() 
	{
		return " 5";
	}

	/* 
	 * OperationID_6() from AirportSecurityI
	 */
	public String OperationID_6() 
	{
		return "";
	}

	/* 
	 * OperationID_7() from AirportSecurityI
	 */
	public String OperationID_7() 
	{
		return "";
	}

	/* 
	 * OperationID_8() from AirportSecurityI
	 */
	public String OperationID_8() 
	{
		return " 8";
	}

	/* 
	 * OperationID_9() from AirportSecurityI
	 */
	public String OperationID_9() 
	{
		return " 9";
	}

	/* 
	 * OperationID_10() from AirportSecurityI
	 */
	public String OperationID_10() 
	{
		return "";
	}
	
	/* 
	 * toString method for debugging
	 */
	public String toString()
	{
		String output = "Moderate_Risk_State";
		return output;
	}
	
	/* 
	 * tightenOrLoosenSecurity from AirportSecurityI
	 */
	public void tightenOrLoosenSecurity(int avg_traffic, int avg_prohibited_items)
	{
		if(!((avg_traffic >= 8) || (avg_prohibited_items >= 2)))
		{
			if(!(((avg_traffic >= 4) && (avg_traffic < 8)) || ((avg_prohibited_items >= 1) && (avg_prohibited_items < 2))))
			{
				if(((avg_traffic >= 0) && (avg_traffic < 4)) || ((avg_prohibited_items >= 0) && (avg_prohibited_items < 1)))
				{
					airport_security.setAirportState(airport_security.low_risk);
					MyLogger.writeMessage("Change to Low Risk State", DebugLevel.STATE_CHANGE);
				}
			}
		}
		if((avg_traffic >= 8) || (avg_prohibited_items >= 2))
		{
			airport_security.setAirportState(airport_security.high_risk);
			MyLogger.writeMessage("Change to High Risk State", DebugLevel.STATE_CHANGE);
		}
//		if(!(((avg_traffic >= 4) && (avg_traffic < 8)) || ((avg_prohibited_items >= 1) && (avg_prohibited_items < 2))))
//		{
//			if(((avg_traffic >= 0) && (avg_traffic < 4)) || ((avg_prohibited_items >= 0) && (avg_prohibited_items < 1)))
//			{
//				airport_security.setAirportState(airport_security.low_risk);
//				MyLogger.writeMessage("Change to Low Risk State", DebugLevel.STATE_CHANGE);
//			}
//		}
		
//		if(!((avg_traffic >= 8) || (avg_prohibited_items >= 2)))
//		{
//			airport_security.setAirportState(airport_security.high_risk);
//		}
	}	
}
