package airportSecurityState.airportStates;

import airportSecurityState.util.MyLogger;
import airportSecurityState.util.Results;
import airportSecurityState.util.MyLogger.DebugLevel;

/**
 * The Class SecurityFactors.
 */
public class SecurityFactors  
{
	
	/** The low risk state */
	AirportStateI low_risk;
	
	/** The moderate risk state. */
	AirportStateI moderate_risk;
	
	/** The high risk state. */
	AirportStateI high_risk;
	
	/** The airportStateI state variable. */
	AirportStateI airportState;
	
	/** String used to concatenate the result generated from the operation performed on the state class. */
	String out_concat_str = "";
	
	/**
	 * securityFactors constructor.
	 */
	public SecurityFactors() 
	{
		low_risk = new Low_Risk(this);
		moderate_risk = new Moderate_Risk(this);
		high_risk = new High_Risk(this);
		airportState = low_risk;
		MyLogger.writeMessage("SecurityFactors Class Constructor has been called", DebugLevel.CONSTRUCTOR);
	}
	
	/**
	 * Sets the SecurityFactors object state.
	 *
	 * @param changeAirportState the new airport state
	 */
	public void setAirportState(AirportStateI changeAirportState)
	{
		airportState = changeAirportState;
	}
	
	/**
	 * Operation ID_1.
	 *
	 * @return the string
	 */
	public String OperationID_1() 
	{
 		return airportState.OperationID_1();
	}

	/**
	 * Operation ID_2.
	 *
	 * @return the string
	 */
	public String OperationID_2() 
	{
		return airportState.OperationID_2();
	}

	/**
	 * Operation ID_3.
	 *
	 * @return the string
	 */
	public String OperationID_3() 
	{
		return airportState.OperationID_3();
	}

	/**
	 * Operation ID_4.
	 *
	 * @return the string
	 */
	public String OperationID_4() 
	{
		return airportState.OperationID_4();
	}

	/**
	 * Operation ID_5.
	 *
	 * @return the string
	 */
	public String OperationID_5() 
	{
		return airportState.OperationID_5();
	}

	/**
	 * Operation ID_6.
	 *
	 * @return the string
	 */
	public String OperationID_6() 
	{
		return airportState.OperationID_6();
	}

	/**
	 * Operation ID_7.
	 *
	 * @return the string
	 */
	public String OperationID_7() 
	{
		return airportState.OperationID_7();
	}

	/**
	 * Operation ID_8.
	 *
	 * @return the string
	 */
	public String OperationID_8() 
	{
		return airportState.OperationID_8();
	}

	/**
	 * Operation ID_9.
	 *
	 * @return the string
	 */
	public String OperationID_9() 
	{
		return airportState.OperationID_9();
	}

	/**
	 * Operation ID_10.
	 *
	 * @return the string
	 */
	public String OperationID_10() 
	{
		return airportState.OperationID_10();
	}
	
	/* 
	 * toString() used for debugging
	 */
	public String toString()
	{
		String output = airportState.toString();
		return output;
	}

	/**
	 * Perform.
	 *
	 * @param r the results class object
	 * @param avg_traffic the avg_traffic
	 * @param avg_prohibited_items the avg_prohibited items
	 */
	public void perform(Results r, int avg_traffic, int avg_prohibited_items) 
	{
		airportState.tightenOrLoosenSecurity(avg_traffic, avg_prohibited_items);
		out_concat_str = OperationID_1();
		out_concat_str += OperationID_2();
		out_concat_str += OperationID_3();
		out_concat_str += OperationID_4();
		out_concat_str += OperationID_5();
		out_concat_str += OperationID_6();
		out_concat_str += OperationID_7();
		out_concat_str += OperationID_8();
		out_concat_str += OperationID_9();
		out_concat_str += OperationID_10();
		MyLogger.writeMessage(out_concat_str, DebugLevel.STATE_OPS);
		out_concat_str += "\n";
		r.setResults(out_concat_str);
	}
}
