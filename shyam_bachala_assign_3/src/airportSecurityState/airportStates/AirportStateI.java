package airportSecurityState.airportStates;

/**
 * AirportStateI Interface.
 */
public interface AirportStateI 
{
	
	/**
	 * Operation ID_1.
	 *
	 * @return string
	 */
	public String OperationID_1();
	
	/**
	 * Operation ID_2.
	 *
	 * @return string
	 */
	public String OperationID_2();
	
	/**
	 * Operation ID_3.
	 *
	 * @return string
	 */
	public String OperationID_3();
	
	/**
	 * Operation ID_4.
	 *
	 * @return string
	 */
	public String OperationID_4();
	
	/**
	 * Operation ID_5.
	 *
	 * @return string
	 */
	public String OperationID_5();
	
	/**
	 * Operation ID_6.
	 *
	 * @return string
	 */
	public String OperationID_6();
	
	/**
	 * Operation ID_7.
	 *
	 * @return string
	 */
	public String OperationID_7();
	
	/**
	 * Operation ID_8.
	 *
	 * @return string
	 */
	public String OperationID_8();
	
	/**
	 * Operation ID_9.
	 *
	 * @return string
	 */
	public String OperationID_9();
	
	/**
	 * Operation ID_10.
	 *
	 * @return string
	 */
	public String OperationID_10();
	
	/**
	 * tightenOrLoosenSecurity.
	 *
	 * @param avg_traffic: total number of travelers / Total number of days
	 * @param avg_prohibited_items: total number of prohibited items / Total number of days
	 */
	public void tightenOrLoosenSecurity(int avg_traffic, int avg_prohibited_items);
}
