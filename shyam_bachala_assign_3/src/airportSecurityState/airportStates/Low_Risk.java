package airportSecurityState.airportStates;

import airportSecurityState.util.MyLogger;
import airportSecurityState.util.MyLogger.DebugLevel;

/**
 * Low_Risk Class.
 */
public class Low_Risk implements AirportStateI 
{
	
	/** SecurityFactors variable */
	SecurityFactors airport_security;
	
	/**
	 * Low risk class constructor.
	 *
	 * @param securityFactors type object
	 */
	public Low_Risk(SecurityFactors securityFactors) 
	{
		airport_security = securityFactors;
		MyLogger.writeMessage("Low Risk State Class Constructor has been called", DebugLevel.CONSTRUCTOR);
	}
	
	/* 
	 * OperationID_1() from AirportSecurityI
	 */
	public String OperationID_1() 
	{
		return "1";
	}

	/* 
	 * OperationID_2() from AirportSecurityI
	 */
	public String OperationID_2() 
	{
		return "";
	}

	/* 
	 * OperationID_3() from AirportSecurityI
	 */
	public String OperationID_3() 
	{
		return " 3";
	}

	/* 
	 * OperationID_4() from AirportSecurityI
	 */
	public String OperationID_4() 
	{
		return "";
	}

	/* 
	 * OperationID_5() from AirportSecurityI
	 */
	public String OperationID_5() 
	{
		return " 5";
	}

	/* 
	 * OperationID_6() from AirportSecurityI
	 */
	public String OperationID_6() 
	{
		return "";
	}

	/* 
	 * OperationID_7() from AirportSecurityI
	 */
	public String OperationID_7() 
	{
		return " 7";
	}

	/* 
	 * OperationID_8() from AirportSecurityI
	 */
	public String OperationID_8() 
	{
		return "";
	}

	/* 
	 * OperationID_9() from AirportSecurityI
	 */
	public String OperationID_9() 
	{
		return " 9";
	}

	/* 
	 * OperationID_10() from AirportSecurityI
	 */
	public String OperationID_10() 
	{
		return "";
	}

	/* 
	 * toString method for debugging
	 */
	public String toString()
	{
		String output = "Low_Risk_State";
		return output;
	}
	
	/* 
	 * tightenOrLoosenSecurity from AirportSecurityI
	 */
	public void tightenOrLoosenSecurity(int avg_traffic, int avg_prohibited_items)
	{
		if(!((avg_traffic >= 8) || (avg_prohibited_items >= 2)))
		{
			if(((avg_traffic >= 4) && (avg_traffic < 8)) || ((avg_prohibited_items >= 1) && (avg_prohibited_items < 2)))
			{	
				airport_security.setAirportState(airport_security.moderate_risk);
				MyLogger.writeMessage("Change to Moderate Risk State", DebugLevel.STATE_CHANGE);
			}
		}
		if((avg_traffic >= 8) || (avg_prohibited_items >= 2))
		{
			airport_security.setAirportState(airport_security.high_risk);
			MyLogger.writeMessage("Change to High Risk State", DebugLevel.STATE_CHANGE);
		}
	}
}
