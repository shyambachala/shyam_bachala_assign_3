package airportSecurityState.util;

/**
 * MyLogger Class.
 */
public class MyLogger 
{
	/*

	DEBUG_VALUE=4 [Print to stdout everytime a constructor is called]
    DEBUG_VALUE=3 [Print to stdout everytime the state is changed]
    DEBUG_VALUE=2 [Print the number of prohibited items, number
    				of days and number of travelers]
    DEBUG_VALUE=1 [Print the operations performed in the states]
    DEBUG_VALUE=0 [No output should be printed from the application to stdout.
                   It is ok to write to the output file though"]
    */

  /**
   * The Enum DebugLevel.
   */
	public static enum DebugLevel 
	{
		/** Nothing is printed in Stdout. */
		RELEASE, 
		/** Change of security state. */
		STATE_CHANGE, 
		/** Information on the state deciding parameter. */
		STATS, 
		/** Operations run on the current state. */
		STATE_OPS, 
		/** Called inside the constructors of classes. */
		CONSTRUCTOR
	};

  /** The debug level. */
	private static DebugLevel debugLevel;


  /**
   * Sets the debug value.
   *
   * @param levelIn the new debug value
   */
	public static void setDebugValue (int levelIn) 
	{
		switch (levelIn) 
		{
			case 4: debugLevel = DebugLevel.CONSTRUCTOR; break;
			case 3: debugLevel = DebugLevel.STATE_CHANGE; break;
			case 2: debugLevel = DebugLevel.STATS; break;
			case 1: debugLevel = DebugLevel.STATE_OPS; break;
			case 0: debugLevel = DebugLevel.RELEASE; break;
		}
	}

  /**
   * Sets the debug value.
   *
   * @param levelIn the new debug value
   */
	public static void setDebugValue (DebugLevel levelIn) 
	{
		debugLevel = levelIn;
	}

  /**
   * Write message.
   *
   * @param message the message
   * @param levelIn the level in
   */
	public static void writeMessage (String  message, DebugLevel levelIn ) 
	{
		if (levelIn == debugLevel)
			System.out.println(message);
	}

  /* 
   * toString() used for debugging
   */
  	public String toString() 
  	{
  		return "Debug Level is " + debugLevel;
  	}
}
