package airportSecurityState.util;

import java.io.File;

/**
 * FileDisplayInterface Interface .
 */
public interface FileDisplayInterface 
{
	
	/**
	 * Write to file.
	 *
	 * @param s the File name
	 */
	void writeToFile(File s);
}
