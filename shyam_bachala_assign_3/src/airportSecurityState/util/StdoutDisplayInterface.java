package airportSecurityState.util;

/**
 * StdoutDisplayInterface Interface .
 */
public interface StdoutDisplayInterface 
{
	
	/**
	 * Write to stdout.
	 */
	void writeToStdout();
}
