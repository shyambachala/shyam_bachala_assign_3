package airportSecurityState.util;
import java.io.File;

import airportSecurityState.util.MyLogger.DebugLevel;

/**
 * Results Class.
 */
public class Results implements FileDisplayInterface, StdoutDisplayInterface
{
	
	/** The private data member. */
	private String record;
	
	/**
	 * New results instance initialization.
	 */
	public Results()
	{
		record = "";
		MyLogger.writeMessage("Results Class Constructor has been called", DebugLevel.CONSTRUCTOR);
	}
	  
	/**
	 * Sets the results data member.
	 *
	 * @param s the string to be set to data member
	 */
	public void setResults(String s)
	{
		record = record + s;
	}
	
	/* writeToFile method
	 * write the results obtained to the output file
	 * @param outf the output file name
	 */
	public void writeToFile(File outf)
	{
		FileProcessor buff_writer = new FileProcessor(outf, false);
		buff_writer.writeOutput(record);
	}
	  
	/* writeToStdout
	 * Print the output obtained to the Stdout
	 */
	public void writeToStdout()
	{
		System.out.println(record);
	}
	
	/* toString method
	 * Debbugger method used to check the output
	 */
	public String toString()
	{
		return record;
	}
}
